package hackerrank.problemSolving;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class EoF {
    public static void main(String[] args) throws FileNotFoundException {
        int lineNo=1;
        Scanner scan = new Scanner(System.in);
        while (scan.hasNext()){
            String line = scan.nextLine();
            System.out.println(lineNo + " " +line);
            lineNo++;
        }
    }
}
