package hackerrank.problemSolving;

import java.util.Scanner;

public class BinaryNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int val = scanner.nextInt();
        String str = Integer.toString(val, 2);
        System.out.println(str);
        int conse = 0;
        int max = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '1'){
                conse++;
                if (conse>max){
                    max = conse;
                }
            }
            else {
                conse = 0;
            }
        }
        System.out.println(max);
    }
}
