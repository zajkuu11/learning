package hackerrank.problemSolving;

import java.util.Scanner;

public class DiagonalDifference {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[][] matrix = new int[n][n];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                matrix[i][j] = scanner.nextInt();
            }
        }

        /*
        *       [0][2]
        *       [1][1]
        *       [2][0]
        * */
        int primaryDiagonal = 0;
        int secondaryDiagonal = 0;
        int temp = matrix.length-1;
        for (int i = 0; i < matrix.length; i++) {
            primaryDiagonal += matrix[i][i];
            secondaryDiagonal += matrix[i][temp];
            temp--;
        }
        System.out.println(primaryDiagonal);
        System.out.println(secondaryDiagonal);
        int result = Math.abs(primaryDiagonal - secondaryDiagonal);
        System.out.println(result);

    }
}
