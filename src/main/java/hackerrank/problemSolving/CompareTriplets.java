package hackerrank.problemSolving;

import java.util.Scanner;

public class CompareTriplets {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] alice_value = new int[3];
        int[] bob_value = new int[3];

        for (int i = 0; i < 3; i++) {
            alice_value[i] = sc.nextInt();
        }
        for (int i = 0; i < 3; i++) {
            bob_value[i] = sc.nextInt();
        }
        int alice_points = 0;
        int bob_points = 0;
        for (int i = 0; i < 3; i++) {
            if (alice_value[i] > bob_value[i]) {
                alice_points++;
            }
            if (alice_value[i] < bob_value[i]){
                bob_points++;
            }
        }
        System.out.println(alice_points + " " + bob_points);
    }
}
