package hackerrank.problemSolving;

import java.util.Scanner;

public class BigSum {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int capacity = sc.nextInt();
        int[] values = new int[capacity];
        long sum = 0;
        for (int i = 0; i < values.length; i++) {
            values[i] = sc.nextInt();
            sum += (long) values[i];
        }
        System.out.println(sum);
    }
}
