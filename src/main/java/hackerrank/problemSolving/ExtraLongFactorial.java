package hackerrank.problemSolving;

import java.math.BigInteger;
import java.util.Scanner;

public class ExtraLongFactorial {
    // Complete the extraLongFactorials function below.
    private static void extraLongFactorials(int n) {
        BigInteger current = BigInteger.ONE;
        for (int j = 1; j <= n; j++) {
            current = current.multiply(BigInteger.valueOf(j));
        }
        System.out.println(current.toString());
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        extraLongFactorials(n);
        scanner.close();
    }
}
