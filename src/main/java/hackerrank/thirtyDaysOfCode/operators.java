package hackerrank.thirtyDaysOfCode;

import java.util.Scanner;

public class operators {
    // Complete the solve function below.
    static void solve(double meal_cost, int tip_percent, int tax_percent) {
        int totalCost;
        double tip = meal_cost * (0.01 * tip_percent);
        System.out.println(Math.round(tip));
        double tax = meal_cost * (0.01 * tax_percent);
        System.out.println(Math.round(tax));
        totalCost = (int) meal_cost + (int) Math.round(tip) + (int) Math.round(tax);

        System.out.println("The total meal cost is " + totalCost + " dollars.");
    }
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        double meal_cost = scanner.nextDouble();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int tip_percent = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int tax_percent = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        solve(meal_cost, tip_percent, tax_percent);

        scanner.close();
    }
}
