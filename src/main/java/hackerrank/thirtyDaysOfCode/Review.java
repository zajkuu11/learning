package hackerrank.thirtyDaysOfCode;

import java.util.Scanner;

public class Review {
    public static void main(String[] args) {
        int T;
        String S;
        Scanner scanner = new Scanner(System.in);
        T = scanner.nextInt();
        scanner.nextLine();

        for (int j = 0; j < T; j++) {
            S = scanner.nextLine();
            for (int i = 0; i < S.length(); i += 2) {
                System.out.print(S.charAt(i));
            }
            System.out.print(" ");
            for (int i = 1; i < S.length(); i += 2) {
                System.out.print(S.charAt(i));
            }

        }


    }
}
